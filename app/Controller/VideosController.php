<?php 
	class VideosController extends AppController {
	    public $helpers = array('Html', 'Form', 'Session');
	    public $components = array('Session');
	    public $uses = array('Video', 'VideoLike', 'VideoComment', 'UserFriend');
	    
	    public function index() {
	        $this->set('video', $this->Video->find('all'));
	    }

		public function pullVideos($userID, $accessToken){
			$this->autoRender = false;
			$this->response->type('json');
									
			$incomingData = object_to_array(json_decode(file_get_contents('https://graph.facebook.com/'.$userID.'/?fields=videos.limit(10).type(uploaded).fields(id,likes,comments)&access_token='.$accessToken)));
			
			$storedVideos = $this->Video->find('all', array(
											'fields' => array('Video.video_id', 'Video.user_id'),
											'conditions' => array('Video.user_id' => $userID)));
																		
			$storedVideoLikes = $this->VideoLike->find('all', array(
													'fields' => array('VideoLike.video_id', 'VideoLike.user_id', 'VideoLike.friend_id'),
													'conditions' => array('VideoLike.user_id' => $userID)));

			$storedVideoComments = $this->VideoComment->find('all', array(
													'fields' => array('VideoComment.comment_id','VideoComment.video_id', 'VideoComment.user_id', 'VideoComment.friend_id'),
													'conditions' => array('VideoComment.user_id' => $userID)));

			$storedFriends = $this->UserFriend->query('SELECT friend_id
													       FROM user_friends AS UserFriend
													   WHERE user_id = '.$userID);
													   
			$storedVideos = reformatStored($storedVideos, 'Video');
			$storedVideoLikes = reformatStored($storedVideoLikes, 'Like');
			$storedVideoComments = reformatStored($storedVideoComments, 'Comment');
			$incomingVideos = array();
			$incomingLikes = array();
			$incomingComments = array();
			$this->reformatIncoming($incomingData, $incomingVideos, $incomingLikes, $incomingComments, $storedFriends, $userID);	
			
			$toAddVideos = arrayRecursiveDiff($incomingVideos, $storedVideos);
			$toRemoveVideos = arrayRecursiveDiff($storedVideos, $incomingVideos);
			$toAddLikes = arrayRecursiveDiff($incomingLikes, $storedVideoLikes);
			$toRemoveLikes = arrayRecursiveDiff($storedVideoLikes, $incomingLikes);
			$toAddComments = arrayRecursiveDiff($incomingComments, $storedVideoComments);
			$toRemoveComments = arrayRecursiveDiff($storedVideoComments, $incomingComments);

			$message = array();
			$message['status'] = 'fail';
			$this->addToDatabase($toAddVideos, 'Video', $message);
			$this->removeFromDatabase($toRemoveVideos, 'Video', $message);
			$this->addToDatabase($toAddLikes, 'Like', $message);
			$this->removeFromDatabase($toRemoveLikes, 'Like', $message);
			$this->addToDatabase($toAddComments, 'Comment', $message);
			$this->removeFromDatabase($toRemoveComments, 'Comment', $message);
			
			$message['status'] = 'success';
			$this->response->body(json_encode($message));
		}
	    
	    private function addToDatabase($toAdd, $type, &$message){
			if(!empty($toAdd)){
				$query = array();
				foreach($toAdd AS $data){
					array_push($query, $data);
				}
				
				if($type == 'Video'){
					$this->Video->getDatasource()->reconnect();
					if($this->Video->saveMany($query)){
						$message['videosAdded'] = 1;
					}
					else{
						$message['videosAdded'] = 0;
						exit;
					}
				}
				else if($type == 'Like'){
					$this->VideoLike->getDatasource()->reconnect();
					if($this->VideoLike->saveMany($query)){
						$message['videoLikesAdded'] = 1;
					}
					else{
						$message['videoLikesAdded'] = 0;
						exit;
					}
				}
				else if($type == 'Comment'){
					$this->VideoComment->getDatasource()->reconnect();
					if($this->VideoComment->saveMany($query)){
						$message['videoCommentsAdded'] = 1;
					}
					else{
						$message['videoCommentsAdded'] = 0;
						exit;
					}
				}
				else{
					$message['addError'] = 'Invalid type add videos';
				}
			}
		}
		
		private function removeFromDatabase($toRemove, $type,  &$message){
			if(!empty($toRemove)){		
				$query = array();		
				if($type == 'Video'){	
					foreach($toRemove AS $data){
						array_push($query, $data['Video']['video_id']);	
					}
					
					$this->Video->getDatasource()->reconnect();
					if($this->Video->deleteAll(array('Video.video_id' => $query)) && (count($query) > 0)) {	
						$message['deletedVideos'] = 1;
					}
				}
				else if($type == 'Like'){
					$this->VideoLike->getDatasource()->reconnect();
					foreach($toRemove AS $data){
						$this->VideoLike->query('DELETE
												 	FROM video_likes
												 WHERE video_id = '.$data['VideoLike']['video_id'].'
												 	   AND user_id = '.$data['VideoLike']['user_id'].'
												 	   AND friend_id = '.$data['VideoLike']['friend_id']);
					}
					$message['deletedVideoLikes'] = 1;
				}
				else if($type == 'Comment'){
					foreach($toRemove AS $data){
						array_push($query, $data['VideoComment']['comment_id']);
					}
					
					$this->VideoComment->getDatasource()->reconnect();
					if($this->VideoComment->deleteAll(array('VideoComment.comment_id' => $query)) && (count($query) > 0)){
						$message['deletedVideoComments'] = 1;
					}
				}
				else{
					$message['removeError'] = 'Invalid type remove videos';
				}
			}
		}

		private function reformatIncoming($incomingData, &$incomingVideos, &$incomingLikes, &$incomingComments, &$storedFriends, $userID){
			$nextUrlVideos = array();
			if(isset($incomingData['videos']['paging']['next'])){
				$nextUrlVideos = $incomingData['videos']['paging']['next'];
			}
			
			if(isset($incomingData['videos']['data']) && !empty($incomingData['videos']['data'])) {
				$incomingData = $incomingData['videos']['data'];	
			}
			
			while(!empty($incomingData) && !isset($incomingData['id'])){
				foreach($incomingData AS $video){			
					$dataVideo = array();								
					$dataVideo['Video'] = array('video_id' => $video['id'],
										   'user_id' => $userID);
						   
					$incomingVideos[$video['id'].$userID] = $dataVideo;
					
					$nextUrlLikes = array();
					if(isset($video['likes']['paging']['next'])){
						$nextUrlLikes = $video['likes']['paging']['next'];
					}					
												
					if(isset($video['likes']['data'])){			
						$incomingDataLikes = $video['likes']['data'];							  
						while(!empty($incomingDataLikes)) {	
							foreach($incomingDataLikes AS $like){					
								$dataLike = array();

								$isFriend = false;
								$friend['UserFriend']['friend_id'] = $like['id'];
								foreach ($storedFriends AS $storedFriend) {
									if($storedFriend === $friend){
										$isFriend = true;
										break;
									}
								}
								if($isFriend){
									$dataLike['VideoLike'] = array('video_id' => $video['id'], 
															   'user_id' => $userID, 
															   'friend_id' => $like['id']);

									$incomingLikes[$video['id'].$like['id']] = $dataLike;
								}
							}
						
							if(!empty($nextUrlLikes)) {
								$incomingDataLikes = object_to_array(json_decode(file_get_contents($nextUrlLikes)));					
								if(isset($incomingDataLikes['paging']['next']))
									$nextUrlLikes = $incomingDataLikes['paging']['next'];
								else
									$nextUrlLikes = array();							
								$incomingDataLikes = $incomingDataLikes['data'];	
							} 
							else {
								$incomingDataLikes = array();
							}
						}
					}
					
					$nextUrlComments = array();
					if(isset($video['comments']['paging']['next'])){
						$nextUrlComments = $video['comments']['paging']['next'];
					}
					
					if(isset($video['comments']['data'])){												
						$incomingDataComments = $video['comments']['data'];							  
						while(!empty($incomingDataComments)) {	
							foreach($incomingDataComments AS $comment){
								$dataComment = array();

								$isFriend = false;
								$friend['UserFriend']['friend_id'] = $comment['from']['id'];
								foreach ($storedFriends AS $storedFriend) {
									if($storedFriend === $friend){
										$isFriend = true;
										break;
									}
								}
								if($isFriend){
									$comment_id = split('_', $comment['id']);
									$comment_id = $comment_id[1];
									$dataComment['VideoComment'] = array('comment_id' => $comment_id,
																'video_id' => $video['id'], 
															   'user_id' => $userID, 
															   'friend_id' => $comment['from']['id']);

									$incomingComments[$video['id'].$comment['from']['id'].$comment_id] = $dataComment;
								}
							}
							
							if(!empty($nextUrlComments)) {
								$incomingDataComments = object_to_array(json_decode(file_get_contents($nextUrlComments)));					
								if(isset($incomingDataComments['paging']['next']))
									$nextUrlComments = $incomingDataComments['paging']['next'];
								else
									$nextUrlComments = array();
								$incomingDataComments = $incomingDataComments['data'];	
							} 
							else {
								$incomingDataComments = array();
							}
						}
					}
				}
				
				if(!empty($nextUrlVideos)) {
					$incomingData = object_to_array(json_decode(file_get_contents($nextUrlVideos)));					
					if(isset($incomingData['paging']['next']))
						$nextUrlVideos = $incomingData['paging']['next'];
					else
						$nextUrlVideos = array();
					$incomingData = $incomingData['data'];	
				} 
				else {
					$incomingData = array();
				}
			}
		}
	    
	}
	
	function object_to_array($data) {
	    if (is_array($data) || is_object($data))
	    {
	        $result = array();
	        foreach ($data as $key => $value)
	        {
	            $result[$key] = object_to_array($value);
	        }
	        return $result;
	    }
	    return $data;
	}
	
	function arrayRecursiveDiff($aArray1, $aArray2) { 
		    $aReturn = array(); 
		   
		    foreach ($aArray1 as $mKey => $mValue) { 
		        if (array_key_exists($mKey, $aArray2)) { 
		            if (is_array($mValue)) { 
		                $aRecursiveDiff = arrayRecursiveDiff($mValue, $aArray2[$mKey]); 
		                if (count($aRecursiveDiff)) {
		                	$aReturn[$mKey] = $aRecursiveDiff;
		                } 
		            } 
		            else { 
		                if ($mValue != $aArray2[$mKey]) { 
		                    $aReturn[$mKey] = $mValue; 
		                } 
		            } 
		        } 
		        else { 
		            $aReturn[$mKey] = $mValue; 
		        } 
		    } 
		   
		    return $aReturn; 
		} 
	
	function reformatStored($storedContent, $type){
		$reformatted = array();	
		if(!empty($storedContent)) {
			foreach($storedContent AS $stored){
				$keyString = "";
				if($type == 'Video')
					$keyString = $stored["Video"]["video_id"].$stored["Video"]["user_id"];
				else if($type == 'Like')
					$keyString = $stored["VideoLike"]["video_id"].$stored["VideoLike"]["friend_id"];
				else if($type == 'Comment')
					$keyString = $stored["VideoComment"]["video_id"].$stored["VideoComment"]["friend_id"].$stored["VideoComment"]["comment_id"];
				else{
					pr('No Valid format for: '.$type);
					exit;
				}
				$reformatted[$keyString] = $stored;
			}
		}
		else {
			$reformatted = array();
		}
		return $reformatted;
	}
?>