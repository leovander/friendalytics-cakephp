<?php 
	class StatusesController extends AppController {
	    public $helpers = array('Html', 'Form', 'Session');
	    public $components = array('Session');
	    public $uses = array('Status', 'StatusLike');
	
	    public function index() {
	        $this->set('status', $this->Status->find('all'));
	    }

		public function pullStatuses($userID, $accessToken){
			$this->autoRender = false;
			$this->response->type('json');
					
			$statuses = object_to_array(json_decode(file_get_contents('https://graph.facebook.com/'.$userID.'/?fields=statuses.fields(likes,message,id)&access_token='.$accessToken)));

			if(isset($statuses['statuses']['paging']['next'])){
				$nextUrl = $statuses['statuses']['paging']['next'];
			}
			
			if(!empty($statuses['statuses']['data'])) {
				$statuses = $statuses['statuses']['data'];	
			}
			
			$overallStatuses = array();
			$overallLikes = 0;
			
			$storedStatuses = $this->Status->find('list', array(
											'fields' => array('Status.statusId', 'Status.userId'),
											'conditions' => array('Status.userId' => $userID)));

			$storedStatusLikes = $this->StatusLike->find('all', array(
													'fields' => array('StatusLike.statusId', 'StatusLike.userId', 'StatusLike.friendId'),
													'conditions' => array('StatusLike.userId' => $userID)));
			
			while(!empty($statuses)) {
				foreach($statuses AS $status){
					if(!array_key_exists($status['id'], $storedStatuses) && !empty($status['id'])){
						$data = array('userId' => $userID,
									  'statusId' => $status['id']);
				        
				        if(isset($status['message'])){
				        	$data['message'] = $status['message'];
				        }
				        
				        if(isset($status['likes']['data'])) {
					        $overallLikes += $this->likes($status['id'], $userID, $status['likes'], $storedStatusLikes);
				        }
				        
				        array_push($overallStatuses, $data);
					}
					else {
				        if(isset($status['likes']['data'])) {
					        $overallLikes += $this->likes($status['id'], $userID, $status['likes'], $storedStatusLikes);
				        }
					}
				}
								
				if(!empty($nextUrl)) {
					$statuses = object_to_array(json_decode(file_get_contents($nextUrl)));
					
					if(isset($statuses['paging']['next'])){
						$nextUrl = $statuses['paging']['next'];
					}
					else {
						$nextUrl = array();
					}
					$statuses = $statuses['data'];	
				} 
				else {
					$statuses = array();
				}
			}
			
			$message = array();
			if($this->Status->saveMany($overallStatuses) && !empty($overallStatuses)) {
				$message['user'] = $userID;
				$message['statusesSubmitted'] = count($overallStatuses);
				$message['statusLikesSubmitted'] = $overallLikes;
				$message['status'] = 'success';
				$this->response->body(json_encode($message));	
			}
			else {
				$message['user'] = $userID;
				$message['statusesSubmitted'] = 0;
				$message['statusLikesSubmitted'] = $overallLikes;
				$message['status'] = 'success';
				$this->response->body(json_encode($message));	
			}
		}
	    
	    private function likes($statusID, $userID, $likes, $storedLikes){
	    	if(isset($likes['paging']['next'])){
				$nextUrl = $likes['paging']['next'];
			}
			
			$likes = $likes['data'];
			$overallLikes = array();
			
			while(!empty($likes)) {	    	
		    	foreach($likes AS $like) {
		    		$data = array('statusId' => $statusID,
			    				  'userId' => $userID,
			    				  'friendId' => $like['id']);
			    	if(!empty($storedLikes)) {
				    	$found = false;
				    	
				    	foreach($storedLikes AS $storedLike) {    
						    if($data === $storedLike['StatusLike']) {
						    	$found = true;
					    	}
				    	}
				    	
				    	if(!$found) {
					    	array_push($overallLikes, $data);
				    	}
			    	} else {		  
			    		array_push($overallLikes, $data);
			    	}
		    	}
		    	
				if(!empty($nextUrl)) {
					$likes = object_to_array(json_decode(file_get_contents($nextUrl)));
					if(isset($likes['paging']['next'])){
						$nextUrl = $likes['paging']['next'];
					}
					else {
						$nextUrl = array();
					}
					$likes = $likes['data'];	
				} 
				else {
					$likes = array();
				}
		    }
		    
		    if($this->StatusLike->saveMany($overallLikes) && !empty($overallLikes)) {
				return count($overallLikes);
			}
			else {
				return count($overallLikes);
			}
		}
	}
	
	function object_to_array($data) {
	    if (is_array($data) || is_object($data))
	    {
	        $result = array();
	        foreach ($data as $key => $value)
	        {
	            $result[$key] = object_to_array($value);
	        }
	        return $result;
	    }
	    return $data;
	}
?>