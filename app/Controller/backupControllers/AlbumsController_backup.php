<?php 
	class AlbumsController extends AppController {
	    public $helpers = array('Html', 'Form', 'Session');
	    public $components = array('Session');
	    public $uses = array('Album', 'AlbumLike');
	
	    public function index() {
	        $this->set('album', $this->Album->find('all'));
	    }

		public function pullAlbums($userID, $accessToken){
			$this->autoRender = false;
			$this->response->type('json');
										
			$albums = object_to_array(json_decode(file_get_contents('https://graph.facebook.com/'.$userID.'/?fields=albums.fields(id,name,link,likes)&access_token='.$accessToken)));
			
			if(isset($albums['albums']['paging']['next'])){
				$nextUrl = $albums['albums']['paging']['next'];
			}
			
			if(!empty($albums['albums']['data'])) {
				$albums = $albums['albums']['data'];	
			}
			
			$overallAlbums = array();
			$overallLikes = 0;
			$message = array();
			
			$storedAlbums = $this->Album->find('list', array(
											'fields' => array('Album.albumId', 'Album.userId'),
											'conditions' => array('Album.userId' => $userID)));
											
			$storedLikedBys = $this->Album->find('all', array(
													'fields' => array('Album.albumId', 'Album.name', 'Album.likedBy'),
													'conditions' => array('Album.userId' => $userID)));
			
			$storedAlbumLikes = $this->AlbumLike->find('all', array(
													'fields' => array('AlbumLike.albumId', 'AlbumLike.userId', 'AlbumLike.friendId'),
													'conditions' => array('AlbumLike.userId' => $userID)));
			
			$updateLikedBys = $this->Album->find('all', array('conditions' => array('Album.userId' => $userID)));
			
			while(!empty($albums)) {
				foreach($albums AS $album){
					$data = array('userId' => $userID,
								  'albumId' => $album['id'],
								  'link' => $album['link'],
								  'likedBy' => '');
									  
					if(!array_key_exists($data['albumId'], $storedAlbums)){								  
				        if(isset($album['name'])){
					        $data['name'] = $album['name'];
				        }
				        
				        if(isset($album['likes']['data'])) {
					        $overallLikes += $this->likes($data, $album['likes'], $storedLikedBys, $storedAlbumLikes, $updateLikedBys, $message);
				        }
				        
				        array_push($overallAlbums, $data);
					}
					else {
				        if(isset($album['likes']['data'])) {
					        $overallLikes += $this->likes($data, $album['likes'], $storedLikedBys, $storedAlbumLikes, $updateLikedBys, $message);
				        }
					}
				}
								
				if(!empty($nextUrl)) {
					$albums = object_to_array(json_decode(file_get_contents($nextUrl)));
					
					if(isset($albums['paging']['next'])){
						$nextUrl = $albums['paging']['next'];
					}
					else {
						$nextUrl = array();
					}
					$albums = $albums['data'];	
				} 
				else {
					$albums = array();
				}
			}
			
			if($this->Album->saveMany($overallAlbums) && !empty($overallAlbums)) {
				$message['user'] = $userID;
				$message['albumsSubmitted'] = count($overallAlbums);
				$message['albumLikesSubmitted'] = $overallLikes;
				$message['status'] = 'success';
				$this->response->body(json_encode($message));	
			}
			else {
				$message['user'] = $userID;
				$message['albumsSubmitted'] = 0;
				$message['albumLikesSubmitted'] = $overallLikes;
				$message['status'] = 'success';
				$this->response->body(json_encode($message));	
			}
		}
	    
	    private function likes(&$albumData, $likes, &$storedLikedBys, &$storedLikes, $updateLikedBys, &$message){
	    	if(isset($likes['paging']['next'])){
				$nextUrl = $likes['paging']['next'];
			}
			
			$likes = $likes['data']; //new fb likes called
			$overallLikes = array(); //all the new objects to be added
			$totalLikes = array(); //id's to compare
			$removedLikes = array();
			
			while(!empty($likes)) {	    		
		    	foreach($likes AS $like) {
		    		$data = array('albumId' => $albumData['albumId'],
			    				  'userId' => $albumData['userId'],
			    				  'friendId' => $like['id']);
			    				  		    				  
			    	if(!empty($storedLikes)) {
				    	$found = false;
				    	
				    	foreach($storedLikes AS $storedLike) {
					    	if($data === $storedLike['AlbumLike']) {
						    	$found = true;
					    	}
					    }
					    
					    if(!$found) {
						    array_push($overallLikes, $data);
						    array_push($totalLikes, $data['friendId']);
					    }
			    	} else {
				    	array_push($overallLikes, $data);
				    	array_push($totalLikes, $data['friendId']);
			    	}
			    	
			    	array_push($totalLikes, $data['friendId']);
		    	}
		    	
				if(!empty($nextUrl)) {
					$likes = object_to_array(json_decode(file_get_contents($nextUrl)));
					if(isset($likes['paging']['next'])){
						$nextUrl = $likes['paging']['next'];
					}
					else {
						$nextUrl = array();
					}
					$likes = $likes['data'];	
				} 
				else {
					$likes = array();
				}
		    }

			if(!empty($storedLikedBys)) {
			    foreach($storedLikedBys AS $storedLikedBy) {
					if($storedLikedBy['Album']['albumId'] == $albumData['albumId']) {
						$likedBys = object_to_array(json_decode($storedLikedBy['Album']['likedBy']));
						
						if(count($likedBys) != count($totalLikes)) {
							if(!empty($likedBys) && !empty($totalLikes)) {
								foreach($likedBys AS $likedBy) {
									$found = false;
									foreach($totalLikes AS $like) {
										if($likedBy == $like) {
											$found = true;
										}
									}
									
									if(!$found) {
										array_push($removedLikes, $likedBy);
									}
								}
							}
						}
					}
			    }
			}
			
			if(!empty($removedLikes)) {
				if($this->AlbumLike->deleteAll(array('AlbumLike.userId' => $albumData['userId'], 'AlbumLike.albumId' => $albumData['albumId'], 'AlbumLike.friendId' => $removedLikes))) {
					$message['albumLikesDeleted'] = count($removedLikes);	
				}
			}
			
			foreach($updateLikedBys AS $updateLikedBy) {
				if($updateLikedBy['Album']['albumId'] == $albumData['albumId']) {
					$this->Album->id = $updateLikedBy['Album']['id'];
					$this->Album->saveField('likedBy', json_encode($totalLikes));		
				}
			}
		    
			if($this->AlbumLike->saveMany($overallLikes) && !empty($overallLikes)) {
				return count($overallLikes);
			}
			else {
				return count($overallLikes);
			}
		}
	}
	
	function object_to_array($data) {
	    if (is_array($data) || is_object($data))
	    {
	        $result = array();
	        foreach ($data as $key => $value)
	        {
	            $result[$key] = object_to_array($value);
	        }
	        return $result;
	    }
	    return $data;
	}
?>