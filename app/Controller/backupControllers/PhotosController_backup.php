<?php 
	class PhotosController extends AppController {
	    public $helpers = array('Html', 'Form', 'Session');
	    public $components = array('Session');
	    public $uses = array('Photo', 'PhotoLike');
	
	    public function index() {
	        $this->set('photos', $this->Photo->find('all'));
	    }

		//pullPhotos will pull all photos from a user along with the photo's info and save it to the database
		public function pullPhotos($userID, $accessToken){
			$this->autoRender = false;
			$this->response->type('json');
							
			$photos = object_to_array(json_decode(file_get_contents('https://graph.facebook.com/'.$userID.'/?fields=photos.type(uploaded).fields(id,source,name,likes,comments)&access_token='.$accessToken)));
			
			//if there is another page of photos we have to go through
			if(isset($photos['photos']['paging']['next'])){
				$nextUrl = $photos['photos']['paging']['next'];
			}
			
			//if photos are not empty
			if(!empty($photos['photos']['data'])) {
				$photos = $photos['photos']['data'];	
			}
			
			$overallPhotos = array();
			$overallLikes = 0;
			
			//Photo Data
			$storedPhotos = $this->Photo->find('list', array(
											'fields' => array('Photo.photoId', 'Photo.userId'),
											'conditions' => array('Photo.userId' => $userID)));
			//PhotoLike Data
			$storedPhotoLikes = $this->PhotoLike->find('all', array(
													'fields' => array('PhotoLike.photoId', 'PhotoLike.userId', 'PhotoLike.friendId'),
													'conditions' => array('PhotoLike.userId' => $userID)));
			//While we still have photos...
			while(!empty($photos)) {
				//go through each photo collecting data
				foreach($photos AS $photo){						
					if(!array_key_exists($photo['id'], $storedPhotos) && !empty($photo['id'])){
						$data = array('userId' => $userID,
									  'photoId' => $photo['id'],
									  'source' => $photo['source']);
				
						//if the photo name is set, set the name
				        if(isset($photo['name'])){
					        $data['name'] = $photo['name'];
				        }
				        //if the photo likes data is set increment count
				        if(isset($photo['likes']['data'])) {
					        $overallLikes += $this->likes($photo['id'], $userID, $photo['likes'], $storedPhotoLikes);
				        }
				        
				        //push data into overallPhotos
				        array_push($overallPhotos, $data);
					}
					else {
				        if(isset($photo['likes']['data'])) {
					        $overallLikes += $this->likes($photo['id'], $userID, $photo['likes'], $storedPhotoLikes);
				        }
					}
				}
				
				if(!empty($nextUrl)) {
					$photos = object_to_array(json_decode(file_get_contents($nextUrl)));
					if(isset($photos['paging']['next'])){
						$nextUrl = $photos['paging']['next'];
					}
					else {
						$nextUrl = array();
					}
					$photos = $photos['data'];	
				} 
				else {
					$photos = array();
				}
			}
			
			$message = array();
			if($this->Photo->saveMany($overallPhotos) && !empty($overallPhotos)) {
				$message['user'] = $userID;
				$message['photosSubmitted'] = count($overallPhotos);
				$message['photoLikesSubmitted'] = $overallLikes;
				$message['status'] = 'success';
				$this->response->body(json_encode($message));	
			}
			else {
				$message['user'] = $userID;
				$message['photosSubmitted'] = 0;
				$message['photoLikesSubmitted'] = $overallLikes;
				$message['status'] = 'success';
				$this->response->body(json_encode($message));	
			}
		}
	    
	    private function likes($photoID, $userID, $likes, $storedLikes){	
	    	if(isset($likes['paging']['next'])){
				$nextUrl = $likes['paging']['next'];
			}
			
			$likes = $likes['data'];
			$overallLikes = array();
			
			while(!empty($likes)) {	    	
		    	foreach($likes AS $like) {
		    		$data = array('photoId' => $photoID,
			    				  'userId' => $userID,
			    				  'friendId' => $like['id']);
			    	if(!empty($storedLikes)) {
				    	$found = false;
				    	
				    	foreach($storedLikes AS $storedLike) {    
						    if($data === $storedLike['PhotoLike']) {
						    	$found = true;
					    	}
				    	}
				    	
				    	if(!$found) {
					    	array_push($overallLikes, $data);
				    	}
			    	} else {		  
			    		array_push($overallLikes, $data);
			    	}
		    	}
		    	
		    	if(!empty($nextUrl)) {
					$likes = object_to_array(json_decode(file_get_contents($nextUrl)));
					if(isset($likes['paging']['next'])){
						$nextUrl = $likes['paging']['next'];
					}
					else {
						$nextUrl = array();
					}
					$likes = $likes['data'];	
				} 
				else {
					$likes = array();
				}
		    }
		    
		    if($this->PhotoLike->saveMany($overallLikes) && !empty($overallLikes)) {
				return count($overallLikes);
			}
			else {
				return count($overallLikes);
			}
		}
	}
	
	function object_to_array($data) {
	    if (is_array($data) || is_object($data))
	    {
	        $result = array();
	        foreach ($data as $key => $value)
	        {
	            $result[$key] = object_to_array($value);
	        }
	        return $result;
	    }
	    return $data;
	}
?>