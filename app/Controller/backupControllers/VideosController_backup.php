<?php 
	class VideosController extends AppController {
	    public $helpers = array('Html', 'Form', 'Session');
	    public $components = array('Session');
	    public $uses = array('Video', 'VideoLike');
	
	    public function index() {
	        $this->set('video', $this->Video->find('all'));
	    }

		public function pullVideos($userID, $accessToken){
			$this->autoRender = false;
			$this->response->type('json');
									
			$videos = object_to_array(json_decode(file_get_contents('https://graph.facebook.com/'.$userID.'/?fields=videos.limit(10).type(uploaded).fields(id,name,description,likes)&access_token='.$accessToken)));

			if(isset($videos['videos']['paging']['next'])){
				$nextUrl = $videos['videos']['paging']['next'];
			}
			
			if(!empty($videos['videos']['data'])) {
				$videos = $videos['videos']['data'];	
			}
			
			$overallVideos = array();
			$overallLikes = 0;
			
			$storedVideos = $this->Video->find('list', array(
											'fields' => array('Video.videoId', 'Video.userId'),
											'conditions' => array('Video.userId' => $userID)));

			$storedVideoLikes = $this->VideoLike->find('all', array(
													'fields' => array('VideoLike.videoId', 'VideoLike.userId', 'VideoLike.friendId'),
													'conditions' => array('VideoLike.userId' => $userID)));
			
			while(!empty($videos)) {
				foreach($videos AS $video){
					if(!array_key_exists($video['id'], $storedVideos) && !empty($video['id'])){
						$data = array('userId' => $userID,
									  'videoId' => $video['id']);
						
						if(isset($video['name'])){
							$data['name'] = $video['name'];
						}						
				        if(isset($video['description'])){
					        $data['description'] = $video['description'];
				        }
				        
				        if(isset($video['likes']['data'])) {
					        $overallLikes += $this->likes($video['id'], $userID, $video['likes'], $storedVideoLikes);
				        }
				        
				        array_push($overallVideos, $data);
					}
					else {
				        if(isset($video['likes']['data'])) {
					        $overallLikes += $this->likes($video['id'], $userID, $video['likes'], $storedVideoLikes);
				        }
					}
				}
								
				if(!empty($nextUrl)) {
					$videos = object_to_array(json_decode(file_get_contents($nextUrl)));
					
					if(isset($videos['paging']['next'])){
						$nextUrl = $videos['paging']['next'];
					}
					else {
						$nextUrl = array();
					}
					$videos = $videos['data'];	
				} 
				else {
					$videos = array();
				}
			}
			
			$message = array();
			if($this->Video->saveMany($overallVideos) && !empty($overallVideos)) {
				$message['user'] = $userID;
				$message['videosSubmitted'] = count($overallVideos);
				$message['videoLikesSubmitted'] = $overallLikes;
				$message['status'] = 'success';
				$this->response->body(json_encode($message));	
			}
			else {
				$message['user'] = $userID;
				$message['videosSubmitted'] = 0;
				$message['videoLikesSubmitted'] = $overallLikes;
				$message['status'] = 'success';
				$this->response->body(json_encode($message));	
			}
		}
	    
	    private function likes($videoID, $userID, $likes, $storedLikes){
	    	if(isset($likes['paging']['next'])){
				$nextUrl = $likes['paging']['next'];
			}
			$likes = $likes['data'];
			
			$overallLikes = array();
			while(!empty($likes)) {	    	
		    	foreach($likes AS $like) {
		    		$data = array('videoId' => $videoID,
			    				  'userId' => $userID,
			    				  'friendId' => $like['id']);
			    	if(!empty($storedLikes)) {
				    	$found = false;
				    	
				    	foreach($storedLikes AS $storedLike) {    
						    if($data === $storedLike['VideoLike']) {
						    	$found = true;
					    	}
				    	}
				    	
				    	if(!$found) {
					    	array_push($overallLikes, $data);
				    	}
			    	} else {		  
			    		array_push($overallLikes, $data);
			    	}
		    	}
		    	
		    	if(!empty($nextUrl)) {
					$likes = object_to_array(json_decode(file_get_contents($nextUrl)));
					if(isset($likes['paging']['next'])){
						$nextUrl = $likes['paging']['next'];
					}
					else {
						$nextUrl = array();
					}
					$likes = $likes['data'];	
				} 
				else {
					$likes = array();
				}
		    }
		    
		    if($this->VideoLike->saveMany($overallLikes) && !empty($overallLikes)) {
				return count($overallLikes);
			}
			else {
				return count($overallLikes);
			}
		}
	}
	
	function object_to_array($data) {
	    if (is_array($data) || is_object($data))
	    {
	        $result = array();
	        foreach ($data as $key => $value)
	        {
	            $result[$key] = object_to_array($value);
	        }
	        return $result;
	    }
	    return $data;
	}
?>