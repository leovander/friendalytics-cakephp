<?php 
	class UsersController extends AppController {
	    public $helpers = array('Html', 'Form', 'Session');
	    public $components = array('Session');
		public $uses = array('User', 'UserFriend', 'AlbumLike', 'PhotoLike', 'StatusLike', 'VideoLike', 'AlbumComment','PhotoComment', 'StatusComment', 'VideoComment'); //add AlbumComment Next
			
	    public function index() {
	        $this->autoRender = false;
	        
	        $user = $this->User->find('all');
	        pr($user);
	    }
		
		public function login($userID, $accessToken){
			$this->autoRender = false;
			$this->response->type('json');
			
			$checkUser = $this->User->find('first', array(
									'conditions' => array('User.user_id' => $userID)));
			if(empty($checkUser)){
				$user = object_to_array(json_decode(file_get_contents('https://graph.facebook.com/'.$userID.'?fields=id,first_name,last_name,name,gender,username,picture.width(50).height(43)&access_token='.$accessToken)));
				
				$this->User->create();
				$data = array('User' => array('user_id' => $user['id'], 'name' => $user['name'], 'firstName' => $user['first_name'], 'lastName' => $user['last_name'], 'profilePictureSmall' => $user['picture']['data']['url'], 'accessToken' => $accessToken, 'isUser' => 1));
				
				$profilePictureLarge = object_to_array(json_decode(file_get_contents('https://graph.facebook.com/'.$userID.'?fields=picture.width(170).height(170).type(square)&access_token='.$accessToken)));
				
				$data['User']['profilePictureLarge'] = $profilePictureLarge['picture']['data']['url'];
				
			    if(isset($user['username'])){
		        	$data['User']['userName'] = $user['username'];
		        }
		        
		        $message = array();
				if($this->User->save($data)) {
					$message['newUser'] = 1;
					$message['status'] = 'success';
					$this->response->body(json_encode($message));	
				}
			}
			else{
				$data = array('user_id' => $checkUser['User']['user_id'], 'isUser' => 1, 'accessToken' => $accessToken);
				
				$message = array();
				if($this->User->save($data)) {
					$message['activatedUser'] = 1;
					$message['status'] = 'success';
					$this->response->body(json_encode($message));	
				}
			}
		}

		public function pullFriends($userID, $accessToken){
			$this->autoRender = false;
			$this->response->type('json');
		    
			$checkUser = $this->User->find('first', array(
									'conditions' => array('User.user_id' => $userID)));
									 
			if(!empty($checkUser)){
				
				$friends = object_to_array(json_decode(file_get_contents('https://graph.facebook.com/'.$userID.'?fields=friends.fields(id,first_name,last_name,username,name,picture.width(50).height(43))&access_token='.$accessToken)));
				
				$largePhoto = object_to_array(json_decode(file_get_contents('https://graph.facebook.com/'.$userID.'?fields=friends.fields(id,picture.width(170).height(170).type(square))&access_token='.$accessToken)));
				$largePhoto = $largePhoto['friends']['data'];
				
				if(!empty($friends)){
					$friends = $friends['friends']['data'];
	        	    
	        	    $overallFriends = array();
	        	    $overallRelations = array();
	        	    $currentPicture = 0;
	        	    
	        	    $storedUsers = $this->User->find('list', array(
	        	    									'fields' => array('User.user_id', 'User.firstName')));
	        	    
	        	    $storedFriends = $this->UserFriend->find('list', array(
	        	    									'fields' => array('UserFriend.friend_id', 'UserFriend.user_id'),
	        	    									'conditions' => array('UserFriend.user_id' => $userID)));
	        	    
			        foreach($friends AS $friend) {
			        	if(!array_key_exists($friend['id'], $storedUsers) && !empty($friend['id'])){
					        $data = array('user_id' => $friend['id'], 'name' => $friend['name'], 'firstName' => $friend['first_name'], 'lastName' => $friend['last_name'], 'profilePictureSmall' => $friend['picture']['data']['url']);
					        
					        $data['profilePictureLarge'] = $largePhoto[$currentPicture]['picture']['data']['url'];
					        
					        if(isset($friend['username'])){
					        	$data['userName'] = $friend['username'];
					        }
					        
					        array_push($overallFriends, $data);
					        
					        $data = array('user_id' => $userID, 'friend_id' => $friend['id']);
					        array_push($overallRelations, $data);
				        } else {
							if(!array_key_exists($friend['id'], $storedFriends) && !empty($friend['id'])) {
						       $data = array('user_id' => $userID, 'friend_id' => $friend['id']);
							   array_push($overallRelations, $data); 
					        }
				        }
				        $currentPicture++;
			        }
			        
			        $message = array();
					if(!empty($overallFriends)) {
						$this->User->getDatasource()->reconnect();
						$this->User->saveMany($overallFriends);
						$this->UserFriend->getDatasource()->reconnect();
						$this->UserFriend->saveMany($overallRelations);
						$message['friendSubmitted'] = count($overallFriends);
						$message['status'] = 'success';
						$this->response->body(json_encode($message));	
					}
					else {
						$message['friendsSubmitted'] = 0;
						$message['status'] = 'success';
						$this->response->body(json_encode($message));	
					}
				}
			}
   		}
   		
   		public function getFriendsData($userID, $sort = null) {
	        $this->autoRender = false;
	        $this->response->type('json');
	        
	        $overallFriends = array();

	        $friends = $this->User->query('SELECT User.user_id, User.name, User.firstName, User.lastName, User.userName, User.profilePictureSmall, User.profilePictureLarge
										       FROM users u
											   INNER JOIN user_friends uf ON uf.user_id=u.user_id
											   INNER JOIN users as User ON User.user_id=uf.friend_id
										   WHERE u.user_id = '.$userID.';');
	        
	        $albumLikes = $this->AlbumLike->query('SELECT COUNT(User.user_id) AS albumLikes, User.user_id
												       FROM users u
													   INNER JOIN album_likes al ON al.user_id=u.user_id
													   INNER JOIN users User ON User.user_id=al.friend_id 
												   WHERE u.user_id = '.$userID.'
												   GROUP BY (User.user_id);');
	        
	        $albumComments = $this->AlbumComment->query('SELECT COUNT(User.user_id) AS albumComments, User.user_id
												       FROM users u
													   INNER JOIN album_comments al ON al.user_id=u.user_id
													   INNER JOIN users User ON User.user_id=al.friend_id 
												   WHERE u.user_id = '.$userID.'
												   GROUP BY (User.user_id);');
	        
	        $photoLikes = $this->PhotoLike->query('SELECT COUNT(User.user_id) AS photoLikes, User.user_id
												       FROM users u
													   INNER JOIN photo_likes al ON al.user_id=u.user_id
													   INNER JOIN users User ON User.user_id=al.friend_id 
												   WHERE u.user_id = '.$userID.'
												   GROUP BY (User.user_id);');
			
			$photoComments = $this->PhotoComment->query('SELECT COUNT(User.user_id) AS photoComments, User.user_id
												       FROM users u
													   INNER JOIN photo_comments al ON al.user_id=u.user_id
													   INNER JOIN users User ON User.user_id=al.friend_id 
												   WHERE u.user_id = '.$userID.'
												   GROUP BY (User.user_id);');

												   
			$statusLikes = $this->StatusLike->query('SELECT COUNT(User.user_id) AS statusLikes, User.user_id
												       FROM users u
													   INNER JOIN status_likes al ON al.user_id=u.user_id
													   INNER JOIN users User ON User.user_id=al.friend_id 
												    WHERE u.user_id = '.$userID.'
												    GROUP BY (User.user_id);');
			
			$statusComments = $this->StatusComment->query('SELECT COUNT(User.user_id) AS statusComments, User.user_id
												       FROM users u
													   INNER JOIN status_comments al ON al.user_id=u.user_id
													   INNER JOIN users User ON User.user_id=al.friend_id 
												    WHERE u.user_id = '.$userID.'
												    GROUP BY (User.user_id);');
													 
			$videoLikes = $this->VideoLike->query('SELECT COUNT(User.user_id) as videoLikes, User.user_id
												       FROM users u
													   INNER JOIN video_likes al ON al.user_id=u.user_id
													   INNER JOIN users User ON User.user_id=al.friend_id 
												  WHERE u.user_id = '.$userID.'
												  GROUP BY (User.user_id);');
												  
			$videoComments = $this->VideoComment->query('SELECT COUNT(User.user_id) as videoComments, User.user_id
												       FROM users u
													   INNER JOIN video_comments al ON al.user_id=u.user_id
													   INNER JOIN users User ON User.user_id=al.friend_id 
												  WHERE u.user_id = '.$userID.'
												  GROUP BY (User.user_id);');									  
	        
	        foreach($friends AS $friend) {		
				$friend['User']['albumLikes'] = 0;
				$friend['User']['photoLikes'] = 0;
				$friend['User']['statusLikes'] = 0;
				$friend['User']['videoLikes'] = 0;
				$friend['User']['totalLikes'] = 0;
				$friend['User']['albumComments'] = 0;
				$friend['User']['photoComments'] = 0;
				$friend['User']['statusComments'] = 0;
				$friend['User']['videoComments'] = 0;
				$friend['User']['totalComments'] = 0;
				
				foreach($albumLikes AS $like) {
					if($friend['User']['user_id'] == $like['User']['user_id']) {
						$friend['User']['albumLikes'] = $like[0]['albumLikes'];
					}
				}
				
				foreach($photoLikes AS $like) {
					if($friend['User']['user_id'] == $like['User']['user_id']) {
						$friend['User']['photoLikes'] = $like[0]['photoLikes'];
					}
				}
				
				foreach($statusLikes AS $like) {
					if($friend['User']['user_id'] == $like['User']['user_id']) {
						$friend['User']['statusLikes'] = $like[0]['statusLikes'];
					}
				}
				
				foreach($videoLikes AS $like) {
					if($friend['User']['user_id'] == $like['User']['user_id']) {
						$friend['User']['videoLikes'] = $like[0]['videoLikes'];
					}
				}
				
				foreach($albumComments AS $comment){
					if($friend['User']['user_id'] == $comment['User']['user_id']) {
						$friend['User']['albumComments'] = $comment[0]['albumComments'];
					}
				}
				
				foreach($photoComments AS $comment){
					if($friend['User']['user_id'] == $comment['User']['user_id']) {
						$friend['User']['photoComments'] = $comment[0]['photoComments'];
					}
				}
				
				foreach($statusComments AS $comment){
					if($friend['User']['user_id'] == $comment['User']['user_id']) {
						$friend['User']['statusComments'] = $comment[0]['statusComments'];
					}
				}
				
				foreach($videoComments AS $comment){
					if($friend['User']['user_id'] == $comment['User']['user_id']) {
						$friend['User']['videoComments'] = $comment[0]['videoComments'];
					}
				}
				
				
				$friend['User']['totalLikes'] = ($friend['User']['albumLikes'] + $friend['User']['photoLikes'] + $friend['User']['statusLikes'] + $friend['User']['videoLikes']);
				$friend['User']['totalComments'] =  ($friend['User']['albumComments'] + $friend['User']['photoComments'] + $friend['User']['statusComments'] + $friend['User']['videoComments']);
				
		    	array_push($overallFriends, $friend);
		    }
	        
	        // TODO
	        if($sort == 'alpha_fn') {
		        usort($overallFriends, function($a, $b)
				{
				    return strcmp($a['User']['firstName'], $b['User']['firstName']);
				});
	        }
	        else if($sort == 'alpha_ln') {
		        usort($overallFriends, function($a, $b)
				{
				    return strcmp($a['User']['lastName'], $b['User']['lastName']);
				});
	        }
	        else {
		        usort($overallFriends, function($a, $b)
				{
				    return (($b['User']['totalLikes']+$b['User']['totalComments']) > ($a['User']['totalLikes'] + $a['User']['totalComments']));
				});
	        }
	        
	        $finalFriends = array();
	        $rank = 1;
	        foreach($overallFriends as $friend) {
		    	$friend['User']['rank'] = $rank;
		    	array_push($finalFriends, $friend);
		    	$rank++;   
	        }
	        
	        $this->response->body(json_encode($finalFriends));
	    }
   		
   		public function doesFriendDataExist($userID){
	   		$this->autoRender = false;
			$this->response->type('json');
			
	   		$friends = $this->UserFriend->find('all', array(
	        	    									'fields' => array('UserFriend.friend_id'),
	        	    									'conditions' => array('UserFriend.user_id' => $userID)));
	   		//function to get friend data
	   		$m = array();
	   		if(!empty($friends)){
	   			$m['hasFriends'] = 1;
		   		$this->response->body(json_encode($m));
	   		}
	   		else{
	   			$m['hasFriends'] = 0;
		   		$this->response->body(json_encode($m));
	   		}	
   		}
	}
	
	function object_to_array($data)
		{
		    if (is_array($data) || is_object($data))
		    {
		        $result = array();
		        foreach ($data as $key => $value)
		        {
		            $result[$key] = object_to_array($value);
		        }
		        return $result;
		    }
		    return $data;
		}
?>