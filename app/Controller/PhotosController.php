<?php 
	class PhotosController extends AppController {
	    public $helpers = array('Html', 'Form', 'Session');
	    public $components = array('Session');
	    public $uses = array('Photo', 'PhotoLike', 'PhotoComment', 'UserFriend');
		
	    public function index() {
	        $this->set('photos', $this->Photo->find('all'));
	    }

		public function pullPhotos($userID, $accessToken){
			
			$this->autoRender = false;
			$this->response->type('json');
							
			$incomingData = object_to_array(json_decode(file_get_contents('https://graph.facebook.com/'.$userID.'/?fields=photos.type(uploaded).fields(id,likes,comments)&access_token='.$accessToken)));
						
			$storedPhotos = $this->Photo->find('all', array(
									'fields' => array('Photo.photo_id', 'Photo.user_id'),
									'conditions' => array('Photo.user_id' => $userID)));

			$storedPhotoLikes = $this->PhotoLike->find('all', array(
									'fields' => array('PhotoLike.photo_id', 'PhotoLike.user_id', 'PhotoLike.friend_id'),
									'conditions' => array('PhotoLike.user_id' => $userID)));
					
			$storedPhotoComments = $this->PhotoComment->find('all', array(
									'fields' => array('PhotoComment.photo_id', 'PhotoComment.user_id', 'PhotoComment.friend_id', 'PhotoComment.comment_id'),
									'conditions' => array('PhotoComment.user_id' => $userID)));
			$storedFriends = $this->UserFriend->query('SELECT friend_id
													       FROM user_friends AS UserFriend
													   WHERE user_id = '.$userID);

			$storedPhotos = reformatStored($storedPhotos, 'Photo');
			$storedPhotoLikes = reformatStored($storedPhotoLikes, 'Like');
			$storedPhotoComments = reformatStored($storedPhotoComments, 'Comment');
			$incomingPhotos = array();
			$incomingLikes = array();
			$incomingComments = array();
			$this->reformatIncoming($incomingData, $incomingPhotos, $incomingLikes, $incomingComments, $storedFriends, $userID);

			$toAddPhotos = arrayRecursiveDiff($incomingPhotos, $storedPhotos);
			$toRemovePhotos = arrayRecursiveDiff($storedPhotos, $incomingPhotos);
			$toAddLikes = arrayRecursiveDiff($incomingLikes, $storedPhotoLikes);
			$toRemoveLikes = arrayRecursiveDiff($storedPhotoLikes, $incomingLikes);
			$toAddComments = arrayRecursiveDiff($incomingComments, $storedPhotoComments);
			$toRemoveComments = arrayRecursiveDiff($storedPhotoComments, $incomingComments);

			$message = array();
			$message['status'] = 'fail';
			$this->addToDatabase($toAddPhotos, 'Photo', $message);
			$this->removeFromDatabase($toRemovePhotos, 'Photo', $message);
			$this->addToDatabase($toAddLikes, 'Like', $message);
			$this->removeFromDatabase($toRemoveLikes, 'Like', $message);
			$this->addToDatabase($toAddComments, 'Comment', $message);
			$this->removeFromDatabase($toRemoveComments, 'Comment', $message);
			
			$message['status'] = 'success';
			$this->response->body(json_encode($message));
		}	

		private function addToDatabase($toAdd, $type, &$message){
			if(!empty($toAdd)){
				$query = array();
				foreach($toAdd AS $data){
					array_push($query, $data);
				}
				if($type == 'Photo'){
				
					$this->Photo->getDatasource()->reconnect();
					if($this->Photo->saveMany($query)){
						$message['photosAdded'] = 1;
					}
					else{
						$message['photosAdded'] = 0;
						exit;
					}
				}
				else if($type == 'Like'){
					$this->PhotoLike->getDatasource()->reconnect();
					if($this->PhotoLike->saveMany($query)){
						$message['photoLikesAdded'] = 1;
					}
					else{
						$message['photoLikesAdded'] = 0;
						exit;
					}
				}
				else if($type == 'Comment'){
					$this->PhotoComment->getDatasource()->reconnect();
					if($this->PhotoComment->saveMany($query)){
						$message['photoCommentsAdded'] = 1;
					}
					else{
						$message['photoCommentsAdded'] = 0;
						exit;
					}
				}
				else{
					$message['addError'] = "invalid type addPhotos";
				}
			}
		}
		
		private function removeFromDatabase($toRemove, $type, &$message){
			if(!empty($toRemove)){		
				$query = array();		
				if($type == 'Photo'){	
					foreach($toRemove AS $data){
						array_push($query, $data['Photo']['photo_id']);	
					}
					
					$this->Photo->getDatasource()->reconnect();
					if($this->Photo->deleteAll(array('Photo.photo_id' => $query)) && (count($query) > 0)) {	
						$message['deletedPhotos'] = 1;
					}
				}
				else if($type == 'Like'){
					$this->PhotoLike->getDatasource()->reconnect();
					foreach($toRemove AS $data){
						$this->PhotoLike->query('DELETE
												 	FROM photo_likes
												 WHERE photo_id = '.$data['PhotoLike']['photo_id'].'
												 	   AND user_id = '.$data['PhotoLike']['user_id'].'
												 	   AND friend_id = '.$data['PhotoLike']['friend_id']);
					}
					$message['deletedPhotoLikes'] = 1;
				}
				else if($type == 'Comment'){
					foreach($toRemove AS $data){
						array_push($query, $data['PhotoComment']['comment_id']);
					}
					$this->PhotoComent->getDatasource()->reonnect();
					if($this->PhotoComment->deleteAll(array('PhotoComment.comment_id' => $query)) && (count($query) > 0)){
						$message['deletedPhotoComments'] = 1;
					}
				}
				else{
					$message['removeError'] = "invalid type remove";
				}
			}
		}
	
		private function reformatIncoming($incomingData, &$incomingPhotos, &$incomingLikes, &$incomingComments, &$storedFriends, $userID){
			$nextUrlPhotos = array();
			if(isset($incomingData['photos']['paging']['next'])){
				$nextUrlPhotos = $incomingData['photos']['paging']['next'];
			}
			
			if(isset($incomingData['photos']['data']) && !empty($incomingData['photos']['data'])) {
				$incomingData = $incomingData['photos']['data'];	
			}
			
			while(!empty($incomingData)  && !isset($incomingData['id'])){
				foreach($incomingData AS $photo){
					$dataPhoto = array();								
					$dataPhoto['Photo'] = array('user_id' => $userID,
										   'photo_id' => $photo['id']);
						   
					$incomingPhotos[$photo['id'].$userID] = $dataPhoto;
					
					$nextUrlLikes = array();
					if(isset($photo['likes']['paging']['next'])){
						$nextUrlLikes = $photo['likes']['paging']['next'];
					}					
												
					if(isset($photo['likes']['data'])){			
						$incomingDataLikes = $photo['likes']['data'];							  
						while(!empty($incomingDataLikes)) {	
							foreach($incomingDataLikes AS $like){					
								$dataLike = array();
								$isFriend = false;
								$friend['UserFriend']['friend_id'] = $like['id'];
								foreach ($storedFriends AS $storedFriend) {
									if($storedFriend === $friend){
										$isFriend = true;
										break;
									}
								}
								if($isFriend){
									$dataLike['PhotoLike'] = array('photo_id' => $photo['id'], 
															   'user_id' => $userID, 
															   'friend_id' => $like['id']);
									$incomingLikes[$photo['id'].$like['id']] = $dataLike;
								}
							}
							
							if(!empty($nextUrlLikes)) {
								$incomingDataLikes = object_to_array(json_decode(file_get_contents($nextUrlLikes)));					
								if(isset($incomingDataLikes['paging']['next']))
									$nextUrlLikes = $incomingDataLikes['paging']['next'];
								else
									$nextUrlLikes = array();							
								$incomingDataLikes = $incomingDataLikes['data'];	
							} 
							else {
								$incomingDataLikes = array();
							}
						}
					}
					
					$nextUrlComments = array();
					if(isset($photo['comments']['paging']['next'])){
						$nextUrlComments = $photo['comments']['paging']['next'];
					}
					
					if(isset($photo['comments']['data'])){												
						$incomingDataComments = $photo['comments']['data'];							  
						while(!empty($incomingDataComments)) {	
							foreach($incomingDataComments AS $comment){
								$dataComment = array();
								$isFriend = false;
								$friend['UserFriend']['friend_id'] = $comment['from']['id'];
								foreach ($storedFriends AS $storedFriend) {
									if($storedFriend === $friend){
										$isFriend = true;
										break;
									}
								}

								if($isFriend){
									$comment_id = split('_', $comment['id']);
									$comment_id = $comment_id[1];
									$dataComment['PhotoComment'] = array('comment_id' => $comment_id,
																'photo_id' => $photo['id'], 
															   'user_id' => $userID, 
															   'friend_id' => $comment['from']['id']);
									$incomingComments[$photo['id'].$comment['from']['id'].$comment_id] = $dataComment;
								}
							}
							
							if(!empty($nextUrlComments)) {
								$incomingDataComments = object_to_array(json_decode(file_get_contents($nextUrlComments)));					
								if(isset($incomingDataComments['paging']['next']))
									$nextUrlComments = $incomingDataComments['paging']['next'];
								else
									$nextUrlComments = array();
								$incomingDataComments = $incomingDataComments['data'];	
							} 
							else {
								$incomingDataComments = array();
							}
						}
					}
				}
				
				if(!empty($nextUrlPhotos)) {
					$incomingData = object_to_array(json_decode(file_get_contents($nextUrlPhotos)));					
					if(isset($incomingData['paging']['next']))
						$nextUrlPhotos = $incomingData['paging']['next'];
					else
						$nextUrlPhotos = array();
					$incomingData = $incomingData['data'];	
				} 
				else {
					$incomingData = array();
				}
			}
		}

	}
	
	function object_to_array($data) {
	    if (is_array($data) || is_object($data))
	    {
	        $result = array();
	        foreach ($data as $key => $value)
	        {
	            $result[$key] = object_to_array($value);
	        }
	        return $result;
	    }
	    return $data;
	}

	function arrayRecursiveDiff($aArray1, $aArray2) { 
	    $aReturn = array(); 
	   
	    foreach ($aArray1 as $mKey => $mValue) { 
	        if (array_key_exists($mKey, $aArray2)) { 
	            if (is_array($mValue)) { 
	                $aRecursiveDiff = arrayRecursiveDiff($mValue, $aArray2[$mKey]); 
	                if (count($aRecursiveDiff)) {
	                	$aReturn[$mKey] = $aRecursiveDiff;
	                } 
	            } 
	            else { 
	                if ($mValue != $aArray2[$mKey]) { 
	                    $aReturn[$mKey] = $mValue; 
	                } 
	            } 
	        } 
	        else { 
	            $aReturn[$mKey] = $mValue; 
	        } 
	    } 
	   
	    return $aReturn; 
	} 
	
	function reformatStored($storedContent, $type){
		$reformatted = array();
		if(!empty($storedContent)) {
			foreach($storedContent AS $stored){
				$keyString = "";
				if($type == 'Photo')
					$keyString = $stored["Photo"]["photo_id"].$stored["Photo"]["user_id"];
				else if($type == 'Like')
					$keyString = $stored["PhotoLike"]["photo_id"].$stored["PhotoLike"]["friend_id"];
				else if($type == 'Comment')
					$keyString = $stored["PhotoComment"]["photo_id"].$stored["PhotoComment"]["friend_id"].$stored["PhotoComment"]["comment_id"];
				else{
					pr('No Valid format for: '.$type);
					exit;
				}
				$reformatted[$keyString] = $stored;
			}
		}
		else {
			$reformatted = array();
		}
		return $reformatted;
	}
?>