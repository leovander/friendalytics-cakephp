<?php 
	class StatusesController extends AppController {
	    public $helpers = array('Html', 'Form', 'Session');
	    public $components = array('Session');
	    public $uses = array('Status', 'StatusLike', 'StatusComment', 'UserFriend');
		
	    public function index() {
	        $this->set('status', $this->Status->find('all'));
	    }

		public function pullStatuses($userID, $accessToken){
			$this->autoRender = false;
			$this->response->type('json');
					
			$incomingData = object_to_array(json_decode(file_get_contents('https://graph.facebook.com/'.$userID.'/?fields=statuses.fields(likes,message,id,comments)&access_token='.$accessToken)));
			
			$storedStatuses = $this->Status->find('all', array(
											'fields' => array('Status.status_id', 'Status.user_id'),
											'conditions' => array('Status.user_id' => $userID)));
			
			$storedStatusLikes = $this->StatusLike->find('all', array(
													'fields' => array('StatusLike.status_id', 'StatusLike.user_id', 'StatusLike.friend_id'),
													'conditions' => array('StatusLike.user_id' => $userID)));
										
			$storedStatusComments = $this->StatusComment->find('all', array(
													'fields' => array('StatusComment.comment_id','StatusComment.status_id', 'StatusComment.user_id', 'StatusComment.friend_id'),
													'conditions' => array('StatusComment.user_id' => $userID)));										
			$storedFriends = $this->UserFriend->query('SELECT friend_id
													       FROM user_friends AS UserFriend
													   WHERE user_id = '.$userID);	
	
			$storedStatuses = reformatStored($storedStatuses, 'Status');
			$storedStatusLikes = reformatStored($storedStatusLikes, 'Like');
			$storedStatusComments = reformatStored($storedStatusComments, 'Comment');
			$incomingStatuses = array();
			$incomingLikes = array();
			$incomingComments = array();
			$this->reformatIncoming($incomingData, $incomingStatuses, $incomingLikes, $incomingComments, $storedFriends, $userID);	
			
			$toAddStatuses = arrayRecursiveDiff($incomingStatuses, $storedStatuses);
			$toRemoveStatuses = arrayRecursiveDiff($storedStatuses, $incomingStatuses);
			$toAddLikes = arrayRecursiveDiff($incomingLikes, $storedStatusLikes);
			$toRemoveLikes = arrayRecursiveDiff($storedStatusLikes, $incomingLikes);
			$toAddComments = arrayRecursiveDiff($incomingComments, $storedStatusComments);
			$toRemoveComments = arrayRecursiveDiff($storedStatusComments, $incomingComments);

			$message = array();
			$message['status'] = 'fail';
			$this->addToDatabase($toAddStatuses, 'Status', $message);
			$this->removeFromDatabase($toRemoveStatuses, 'Status', $message);
			$this->addToDatabase($toAddLikes, 'Like', $message);
			$this->removeFromDatabase($toRemoveLikes, 'Like', $message);
			$this->addToDatabase($toAddComments, 'Comment', $message);
			$this->removeFromDatabase($toRemoveComments, 'Comment', $message);
			
			$message['status'] = 'success';
			$this->response->body(json_encode($message));
		}
		
		private function addToDatabase($toAdd, $type, &$message){
			if(!empty($toAdd)){
				$query = array();
				foreach($toAdd AS $data){
					array_push($query, $data);
				}
				
				if($type == 'Status'){
					$this->Status->getDatasource()->reconnect();
					if($this->Status->saveMany($query)){
						$message['statusesAdded'] = 1;
 					}
					else{
					$message['statusesAdded'] = 0;
						exit;
					}
				}
				else if($type == 'Like'){
					$this->StatusLike->getDatasource()->reconnect();
					if($this->StatusLike->saveMany($query)){
						$message['statusLikesAdded'] = 1;
					}
					else{
						$message['statusLikesAdded'] = 0;
						exit;
					}
				}
				else if($type == 'Comment'){
					$this->StatusComment->getDatasource()->reconnect();
					if($this->StatusComment->saveMany($query)){
						$message['statusCommentsAdded'] = 1;
					}
					else{
						$message['statusCommentsAdded'] = 0;
						exit;
					}
				}
				else{
					$message['addError'] = "invalid datatype statusAdd";
				}
			}
		}
		
		private function removeFromDatabase($toRemove, $type, &$message){
			if(!empty($toRemove)){		
				$query = array();		
				if($type == 'Status'){	
					$this->Status->getDatasource()->reconnect();
					foreach($toRemove AS $data){
						array_push($query, $data['Status']['status_id']);	
					}
					if($this->Status->deleteAll(array('Status.status_id' => $query)) && (count($query) > 0)) {	
						$message['statusesRemoved'] = 1;
					}
				}
				else if($type == 'Like'){
				$this->StatusLike->getDatasource()->reconnect();
					foreach($toRemove AS $data){
						$this->StatusLike->query('DELETE
												 	FROM status_likes
												 WHERE status_id = '.$data['StatusLike']['status_id'].'
												 	   AND user_id = '.$data['StatusLike']['user_id'].'
												 	   AND friend_id = '.$data['StatusLike']['friend_id']);
					}
					$message['statusLikesRemove'] = 1;
				}
				else if($type == 'Comment'){
					$this->StatusComment->getDatasource()->reconnect();
					foreach($toRemove AS $data){
						array_push($query, $data['StatusComment']['comment_id']);
					}
					if($this->StatusComment->deleteAll(array('StatusComment.comment_id' => $query)) && (count($query) > 0)){
						$message['statusCommentRemove'] = 1;
					}
				}
				else{
					$message['removeError'] = "invalid type statusRemove";
				}
			}
		}

		private function reformatIncoming($incomingData, &$incomingStatuses, &$incomingLikes, &$incomingComments, &$storedFriends, $userID){
			$nextUrlStatuses = array();
			if(isset($incomingData['statuses']['paging']['next'])){
				$nextUrlStatuses = $incomingData['statuses']['paging']['next'];
			}
			
			if(isset($incomingData['statuses']['data']) && !empty($incomingData['statuses']['data'])) {
				$incomingData = $incomingData['statuses']['data'];	
			}
			
			while(!empty($incomingData) && !isset($incomingData['id'])){
				foreach($incomingData AS $status){			
					$dataStatus = array();								
					$dataStatus['Status'] = array('status_id' => $status['id'],
										   'user_id' => $userID);
						   
					$incomingStatuses[$status['id'].$userID] = $dataStatus;
					
					$nextUrlLikes = array();
					if(isset($status['likes']['paging']['next'])){
						$nextUrlLikes = $status['likes']['paging']['next'];
					}					
												
					if(isset($status['likes']['data'])){			
						$incomingDataLikes = $status['likes']['data'];							  
						while(!empty($incomingDataLikes)) {	
							foreach($incomingDataLikes AS $like){					
								$dataLike = array();

								$isFriend = false;
								$friend['UserFriend']['friend_id'] = $like['id'];
								foreach ($storedFriends AS $storedFriend) {
									if($storedFriend === $friend){
										$isFriend = true;
										break;
									}
								}
								if($isFriend){
									$dataLike['StatusLike'] = array('status_id' => $status['id'], 
															   'user_id' => $userID, 
															   'friend_id' => $like['id']);
									
									$incomingLikes[$status['id'].$like['id']] = $dataLike;
								}
							}
							
							if(!empty($nextUrlLikes)) {
								$incomingDataLikes = object_to_array(json_decode(file_get_contents($nextUrlLikes)));					
								if(isset($incomingDataLikes['paging']['next']))
									$nextUrlLikes = $incomingDataLikes['paging']['next'];
								else
									$nextUrlLikes = array();							
								$incomingDataLikes = $incomingDataLikes['data'];	
							} 
							else {
								$incomingDataLikes = array();
							}
						}
					}
					
					$nextUrlComments = array();
					if(isset($status['comments']['paging']['next'])){
						$nextUrlComments = $status['comments']['paging']['next'];
					}
					
					if(isset($status['comments']['data'])){												
						$incomingDataComments = $status['comments']['data'];							  
						while(!empty($incomingDataComments)) {	
							foreach($incomingDataComments AS $comment){
								$dataComment = array();

								$isFriend = false;
								$friend['UserFriend']['friend_id'] = $comment['from']['id'];
								foreach ($storedFriends AS $storedFriend) {
									if($storedFriend === $friend){
										$isFriend = true;
										break;
									}
								}
								if($isFriend){
									$comment_id = split('_', $comment['id']);
									$comment_id = $comment_id[1];
									$dataComment['StatusComment'] = array('comment_id' => $comment_id,
																'status_id' => $status['id'], 
															   'user_id' => $userID, 
															   'friend_id' => $comment['from']['id']);
									$incomingComments[$status['id'].$comment['from']['id'].$comment_id] = $dataComment;
								}
							}
							
							if(!empty($nextUrlComments)) {
								$incomingDataComments = object_to_array(json_decode(file_get_contents($nextUrlComments)));					
								if(isset($incomingDataComments['paging']['next']))
									$nextUrlComments = $incomingDataComments['paging']['next'];
								else
									$nextUrlComments = array();
								$incomingDataComments = $incomingDataComments['data'];	
							} 
							else {
								$incomingDataComments = array();
							}
						}
					}
				}
				
				if(!empty($nextUrlStatuses)) {
					$incomingData = object_to_array(json_decode(file_get_contents($nextUrlStatuses)));					
					if(isset($incomingData['paging']['next']))
						$nextUrlStatuses = $incomingData['paging']['next'];
					else
						$nextUrlStatuses = array();
					$incomingData = $incomingData['data'];	
				} 
				else {
					$incomingData = array();
				}
			}
		}
	}
	
	function object_to_array($data) {
	    if (is_array($data) || is_object($data))
	    {
	        $result = array();
	        foreach ($data as $key => $value)
	        {
	            $result[$key] = object_to_array($value);
	        }
	        return $result;
	    }
	    return $data;
	}
	
	function arrayRecursiveDiff($aArray1, $aArray2) { 
		    $aReturn = array(); 
		   
		    foreach ($aArray1 as $mKey => $mValue) { 
		        if (array_key_exists($mKey, $aArray2)) { 
		            if (is_array($mValue)) { 
		                $aRecursiveDiff = arrayRecursiveDiff($mValue, $aArray2[$mKey]); 
		                if (count($aRecursiveDiff)) {
		                	$aReturn[$mKey] = $aRecursiveDiff;
		                } 
		            } 
		            else { 
		                if ($mValue != $aArray2[$mKey]) { 
		                    $aReturn[$mKey] = $mValue; 
		                } 
		            } 
		        } 
		        else { 
		            $aReturn[$mKey] = $mValue; 
		        } 
		    } 
		   
		    return $aReturn; 
		} 
	
	function reformatStored($storedContent, $type){
		$reformatted = array();	
		if(!empty($storedContent)) {
			foreach($storedContent AS $stored){
				$keyString = "";
				if($type == 'Status')
					$keyString = $stored["Status"]["status_id"].$stored["Status"]["user_id"];
				else if($type == 'Like')
					$keyString = $stored["StatusLike"]["status_id"].$stored["StatusLike"]["friend_id"];
				else if($type == 'Comment')
					$keyString = $stored["StatusComment"]["status_id"].$stored["StatusComment"]["friend_id"].$stored["StatusComment"]["comment_id"];
				else{
					pr('No Valid format for: '.$type);
					exit;
				}
				$reformatted[$keyString] = $stored;
			}
		}
		else {
			$reformatted = array();
		}
		return $reformatted;
	}

?>