<?php 
	class AlbumsController extends AppController {
	    public $helpers = array('Html', 'Form', 'Session');
	    public $components = array('Session');
	    public $uses = array('Album', 'AlbumLike', 'AlbumComment', 'UserFriend');
	
	    public function index() {
	        $this->set('album', $this->Album->find('all'));
	    }
	    
		public function pullAlbums($userID, $accessToken){
			$this->autoRender = false;
			$this->response->type('json');
									
			$incomingData = object_to_array(json_decode(file_get_contents('https://graph.facebook.com/'.$userID.'/?fields=albums.fields(id,likes,comments)&access_token='.$accessToken)));
						
			$storedAlbums = $this->Album->find('all', array(
								'fields' => array('Album.album_id', 'Album.user_id'),
								'conditions' => array('Album.user_id' => $userID)));								

			$storedAlbumLikes = $this->AlbumLike->find('all', array(
								'fields' => array('AlbumLike.album_id', 'AlbumLike.user_id', 'AlbumLike.friend_id'),
								'conditions' => array('AlbumLike.user_id' => $userID)));

			$storedAlbumComments = $this->AlbumComment->find('all', array(
								'fields' => array('AlbumComment.comment_id', 'AlbumComment.album_id', 'AlbumComment.user_id', 'AlbumComment.friend_id'),
								'conditions' => array('AlbumComment.user_id' => $userID)));

			$storedFriends = $this->UserFriend->query('SELECT friend_id
													       FROM user_friends AS UserFriend
													   WHERE user_id = '.$userID);

			$storedAlbums = reformatStored($storedAlbums, 'Album');
			$storedAlbumLikes = reformatStored($storedAlbumLikes, 'Like');
			$storedAlbumComments = reformatStored($storedAlbumComments, 'Comment');
			$incomingAlbums = array();
			$incomingLikes = array();
			$incomingComments = array();
			$this->reformatIncoming($incomingData, $incomingAlbums, $incomingLikes, $incomingComments, $storedFriends, $userID);		
			
			$toAddAlbums = arrayRecursiveDiff($incomingAlbums, $storedAlbums);
			$toRemoveAlbums = arrayRecursiveDiff($storedAlbums, $incomingAlbums);
			$toAddLikes = arrayRecursiveDiff($incomingLikes, $storedAlbumLikes);
			$toRemoveLikes = arrayRecursiveDiff($storedAlbumLikes, $incomingLikes);
			$toAddComments = arrayRecursiveDiff($incomingComments, $storedAlbumComments);
			$toRemoveComments = arrayRecursiveDiff($storedAlbumComments, $incomingComments);

			$message = array();
			$message['status'] = 'fail';
			$this->addToDatabase($toAddAlbums, 'Album', $message);
			$this->removeFromDatabase($toRemoveAlbums, 'Album', $message);
			$this->addToDatabase($toAddLikes, 'Like', $message);
			$this->removeFromDatabase($toRemoveLikes, 'Like', $message);
			$this->addToDatabase($toAddComments, 'Comment', $message);
			$this->removeFromDatabase($toRemoveComments, 'Comment', $message);
			 			
			$message['status'] = 'success';
			$this->response->body(json_encode($message));	
		}
		
		private function addToDatabase($toAdd, $type, &$message){
			if(!empty($toAdd)){
				$query = array();
				foreach($toAdd AS $data){
					array_push($query, $data);
				}				
				if($type == 'Album'){
					$this->Album->getDatasource()->reconnect();
					if($this->Album->saveMany($query)){
						$message['albumsAdded'] = 1;
					}
					else{
						$message['albumsAdded'] = 0;
						exit;
					}
				}
				else if($type == 'Like'){
					$this->AlbumLike->getDatasource()->reconnect();
					if($this->AlbumLike->saveMany($query)){
						$message['albumLikesAdded'] = 1;
					}
					else{
						$message['albumLikessAdded'] = 0;
						exit;
					}
				}
				else if($type == 'Comment'){
					$this->AlbumComment->getDatasource()->reconnect();
					if($this->AlbumComment->saveMany($query)){
						$message['albumCommentsAdded'] = 1;
					}
					else{
						$message['albumCommentsAdded'] = 0;
						exit;
					}
				}
				else{
					$message['addError'] = 'Invalid type add albums';
				}
			}
		}
		
		private function removeFromDatabase($toRemove, $type, &$message){
			if(!empty($toRemove)){		
				$query = array();		
				if($type == 'Album'){	
					foreach($toRemove AS $data){
						array_push($query, $data['Album']['album_id']);	
					}
					
					$this->Album->getDatasource()->reconnect();
					if($this->Album->deleteAll(array('Album.album_id' => $query)) && (count($query) > 0)) {	
						$message['deletedAlbums'] = 1;
					}
				}
				else if($type == 'Like'){
					$this->AlbumLike->getDatasource()->reconnect();
					foreach($toRemove AS $data){
						$this->AlbumLike->query('DELETE
												 	FROM album_likes
												 WHERE album_id = '.$data['AlbumLike']['album_id'].'
												 	   AND user_id = '.$data['AlbumLike']['user_id'].'
												 	   AND friend_id = '.$data['AlbumLike']['friend_id']);
					}
					$message['deletedAlbumLikes'] = 1;
					
				}
				else if($type == 'Comment'){
					foreach($toRemove AS $data){
						array_push($query, $data['AlbumComment']['comment_id']);
					}
					
					$this->AlbumComment->getDatasource()->reconnect();
					if($this->AlbumComment->deleteAll(array('AlbumComment.comment_id' => $query)) && (count($query) > 0)){
						$message['deletedAlbumComments'] = 1;
					}
				}
				else{
					$message['removeError'] = 'Invalid type remove albums';
				}
			}
		}

		private function reformatIncoming($incomingData, &$incomingAlbums, &$incomingLikes, &$incomingComments, &$storedFriends, $userID){
			$nextUrlAlbums = array();
			if(isset($incomingData['albums']['paging']['next'])){
				$nextUrlAlbums = $incomingData['albums']['paging']['next'];
			}

			if(isset($incomingData['albums']['data']) && !empty($incomingData['albums']['data'])) {
				$incomingData = $incomingData['albums']['data'];	
			}
			
			while(!empty($incomingData) && !isset($incomingData['id'])){
				foreach($incomingData AS $album){
					$dataAlbum = array();								
					$dataAlbum['Album'] = array('user_id' => $userID,
										   'album_id' => $album['id']);
						   
					$incomingAlbums[$album['id'].$userID] = $dataAlbum;
					$nextUrlLikes = array();
					if(isset($album['likes']['paging']['next'])){
						$nextUrlLikes = $album['likes']['paging']['next'];
					}					
												
					if(isset($album['likes']['data'])){			
						$incomingDataLikes = $album['likes']['data'];							  
						while(!empty($incomingDataLikes)) {	
							foreach($incomingDataLikes AS $like){
												
								$dataLike = array();

								$isFriend = false;
								$friend['UserFriend']['friend_id'] = $like['id'];
								foreach ($storedFriends AS $storedFriend) {
									if($storedFriend === $friend){
										$isFriend = true;
										break;
									}
								}
								if($isFriend){
									$dataLike['AlbumLike'] = array('album_id' => $album['id'], 
															   'user_id' => $userID, 
															   'friend_id' => $like['id']);
									$incomingLikes[$album['id'].$like['id']] = $dataLike;
								}
							}
							
							if(!empty($nextUrlLikes)) {
								$incomingDataLikes = object_to_array(json_decode(file_get_contents($nextUrlLikes)));					
								if(isset($incomingDataLikes['paging']['next']))
									$nextUrlLikes = $incomingDataLikes['paging']['next'];
								else
									$nextUrlLikes = array();							
								$incomingDataLikes = $incomingDataLikes['data'];	
							} 
							else {
								$incomingDataLikes = array();
							}
						}
					}
					
					$nextUrlComments = array();
					if(isset($album['comments']['paging']['next'])){
						$nextUrlComments = $album['comments']['paging']['next'];
					}
					
					if(isset($album['comments']['data'])){												
						$incomingDataComments = $album['comments']['data'];							  
						while(!empty($incomingDataComments)) {	
							foreach($incomingDataComments AS $comment){
								$dataComment = array();

								$isFriend = false;
								$friend['UserFriend']['friend_id'] = $comment['from']['id'];
								foreach ($storedFriends AS $storedFriend) {
									if($storedFriend === $friend){
										$isFriend = true;
										break;
									}
								}
								if($isFriend){
									$comment_id = split('_', $comment['id']);
									$comment_id = $comment_id[1];
									$dataComment['AlbumComment'] = array('comment_id' => $comment_id,
																'album_id' => $album['id'], 
															   'user_id' => $userID, 
															   'friend_id' => $comment['from']['id']);
									$incomingComments[$album['id'].$comment['from']['id'].$comment_id] = $dataComment;
								}
							}
							
							if(!empty($nextUrlComments)) {
								$incomingDataComments = object_to_array(json_decode(file_get_contents($nextUrlComments)));					
								if(isset($incomingDataComments['paging']['next']))
									$nextUrlComments = $incomingDataComments['paging']['next'];
								else
									$nextUrlComments = array();
								$incomingDataComments = $incomingDataComments['data'];	
							} 
							else {
								$incomingDataComments = array();
							}
						}
					}
				}
				
				if(!empty($nextUrlAlbums)) {
					$incomingData = object_to_array(json_decode(file_get_contents($nextUrlAlbums)));					
					if(isset($incomingData['paging']['next']))
						$nextUrlAlbums = $incomingData['paging']['next'];
					else
						$nextUrlAlbums = array();
					$incomingData = $incomingData['data'];	
				} 
				else {
					$incomingData = array();
				}
			}
		}
	}
	
	function object_to_array($data) {
	    if (is_array($data) || is_object($data))
	    {
	        $result = array();
	        foreach ($data as $key => $value)
	        {
	            $result[$key] = object_to_array($value);
	        }
	        return $result;
	    }
	    return $data;
	}
	
	function arrayRecursiveDiff($aArray1, $aArray2) { 
		    $aReturn = array(); 
		   
		    foreach ($aArray1 as $mKey => $mValue) { 
		        if (array_key_exists($mKey, $aArray2)) { 
		            if (is_array($mValue)) { 
		                $aRecursiveDiff = arrayRecursiveDiff($mValue, $aArray2[$mKey]); 
		                if (count($aRecursiveDiff)) {
		                	$aReturn[$mKey] = $aRecursiveDiff;
		                } 
		            } 
		            else { 
		                if ($mValue != $aArray2[$mKey]) { 
		                    $aReturn[$mKey] = $mValue; 
		                } 
		            } 
		        } 
		        else { 
		            $aReturn[$mKey] = $mValue; 
		        } 
		    } 
		   
		    return $aReturn; 
		} 
	
	function reformatStored($storedContent, $type){
		$reformatted = array();	
		if(!empty($storedContent)) {
			foreach($storedContent AS $stored){
				$keyString = "";
				if($type == 'Album')
					$keyString = $stored["Album"]["album_id"].$stored["Album"]["user_id"];
				else if($type == 'Like')
					$keyString = $stored["AlbumLike"]["album_id"].$stored["AlbumLike"]["friend_id"];
				else if($type == 'Comment')
					$keyString = $stored["AlbumComment"]["album_id"].$stored["AlbumComment"]["friend_id"].$stored["AlbumComment"]["comment_id"];
				else{
					pr('No Valid format for: '.$type);
					exit;
				}
				$reformatted[$keyString] = $stored;
			}
		}
		else {
			$reformatted = array();
		}
		return $reformatted;
	}

?>