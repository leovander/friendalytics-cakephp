#Friendalytics RDB 0.2.6
#RDB created by Israel Torres

DROP TABLE video_comments;
DROP TABLE video_likes;
DROP TABLE videos;
DROP TABLE status_comments;
DROP TABLE status_likes;
DROP TABLE statuses;
DROP TABLE photo_comments;
DROP TABLE photo_likes;
DROP TABLE photos;
DROP TABLE album_comments;
DROP TABLE album_likes;
DROP TABLE albums;
DROP TABLE user_friends;
DROP TABLE users;

CREATE TABLE users (
	user_id varchar(255) NOT NULL,
 	name varchar(255) DEFAULT NULL,
  	firstName varchar(255) NOT NULL,
  	lastName varchar(255) NOT NULL,
  	userName varchar(255) DEFAULT NULL,
  	profilePictureSmall varchar(255) NOT NULL,
  	profilePictureLarge varchar(255) NOT NULL,
  	accessToken varchar(255) DEFAULT NULL,
  	isUser tinyint(4) NOT NULL DEFAULT '0',
  	created datetime NOT NULL,
  	modified datetime NOT NULL,
  	PRIMARY KEY users_PK (user_id),
  	UNIQUE KEY user_id_UK (user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE user_friends (
  	user_id varchar(255) NOT NULL,
  	friend_id varchar(255) NOT NULL,
  	PRIMARY KEY user_friends_PK (user_id, friend_id),
  	FOREIGN KEY user_friends_users_FK (user_id) REFERENCES users (user_id) ON DELETE CASCADE,
  	FOREIGN KEY user_friends_users_FK (friend_id) REFERENCES users (user_id) ON DELETE CASCADE,
  	UNIQUE KEY  user_friends_UK (user_id, friend_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE albums (
  	album_id varchar(255) NOT NULL,
  	user_id varchar(255) NOT NULL,
  	name text DEFAULT NULL,
  	link varchar(255) NOT NULL,
  	PRIMARY KEY album_id_PK (album_id, user_id),
  	FOREIGN KEY album_id_users_FK (user_id) REFERENCES users (user_id) ON DELETE CASCADE  
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE album_likes (
  	album_id varchar(255) NOT NULL,
  	user_id varchar(255) NOT NULL,
  	friend_id varchar(255) NOT NULL,
  	PRIMARY KEY album_likes_PK (album_id, user_id, friend_id),
  	FOREIGN KEY album_likes_albums_FK (album_id) REFERENCES albums (album_id) ON DELETE CASCADE,
  	FOREIGN KEY album_likes_user_id_FK (user_id) REFERENCES users (user_id),
  	FOREIGN KEY album_likes_friends_FK (friend_id) REFERENCES users (user_id) ON DELETE CASCADE,
  	UNIQUE KEY album_likes_UK (album_id, friend_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE album_comments (
  	comment_id varchar(255) NOT NULL,
  	album_id varchar(255) NOT NULL,
  	user_id varchar(255) NOT NULL,
  	friend_id varchar(255) NOT NULL,
  	PRIMARY KEY album_comments_PK (comment_id, album_id, user_id, friend_id),
  	FOREIGN KEY album_comments_albums_FK (album_id) REFERENCES albums (album_id) ON DELETE CASCADE,
  	FOREIGN KEY album_comments_users_FK (user_id) REFERENCES users (user_id),
  	FOREIGN KEY album_comments_friends_FK (friend_id) REFERENCES users (user_id) ON DELETE CASCADE,
  	UNIQUE KEY photo_likes_UK (comment_id, friend_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE photos (
  	photo_id varchar(255) NOT NULL,
  	user_id varchar(255) NOT NULL,
  	source varchar(255) NOT NULL,
  	PRIMARY KEY (photo_id, user_id),
  	FOREIGN KEY photo_id_users_FK (user_id) REFERENCES users (user_id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE photo_likes (
	photo_id varchar(255) NOT NULL,
  	user_id varchar(255) NOT NULL,
  	friend_id varchar(255) NOT NULL,
  	PRIMARY KEY photo_likes_PK (photo_id, user_id, friend_id),
  	FOREIGN KEY photo_likes_photos_FK (photo_id) REFERENCES photos (photo_id) ON DELETE CASCADE,
  	FOREIGN KEY photo_likes_user_id_FK (user_id) REFERENCES users (user_id),
  	FOREIGN KEY photo_likes_friends_FK (friend_id) REFERENCES users (user_id) ON DELETE CASCADE,
  	UNIQUE KEY photo_likes_UK (photo_id, friend_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE photo_comments (
  	comment_id varchar(255) NOT NULL,
  	photo_id varchar(255) NOT NULL,
  	user_id varchar(255) NOT NULL,
  	friend_id varchar(255) NOT NULL,
  	PRIMARY KEY photo_comments_PK (comment_id, photo_id, user_id, friend_id),
  	FOREIGN KEY photo_comments_photos_FK (photo_id) REFERENCES photos (photo_id) ON DELETE CASCADE,
  	FOREIGN KEY photo_comments_users_FK (user_id) REFERENCES users (user_id),
  	FOREIGN KEY photo_comments_friends_FK (friend_id) REFERENCES users (user_id) ON DELETE CASCADE,
  	UNIQUE KEY photo_comment_UK (comment_id, friend_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE statuses (
  	status_id varchar(255) NOT NULL,
  	user_id varchar(255) NOT NULL,
  	message text DEFAULT NULL,
  	PRIMARY KEY (status_id, user_id),
  	FOREIGN KEY status_id_users_FK (user_id) REFERENCES users (user_id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE status_likes (
	status_id varchar(255) NOT NULL,
  	user_id varchar(255) NOT NULL,
  	friend_id varchar(255) NOT NULL,
  	PRIMARY KEY status_likes_PK (status_id, user_id, friend_id),
  	FOREIGN KEY status_likes_photos_FK (status_id) REFERENCES statuses (status_id) ON DELETE CASCADE,
  	FOREIGN KEY status_likes_user_id_FK (user_id) REFERENCES users (user_id),
  	FOREIGN KEY status_likes_friends_FK (friend_id) REFERENCES users (user_id) ON DELETE CASCADE,
  	UNIQUE KEY status_likes_UK (status_id, friend_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE status_comments (
	comment_id varchar(255) NOT NULL,
	status_id varchar(255) NOT NULL,
  	user_id varchar(255) NOT NULL,
  	friend_id varchar(255) NOT NULL,
  	PRIMARY KEY status_comments_PK (comment_id, status_id, user_id, friend_id),
  	FOREIGN KEY status_comments_photos_FK (status_id) REFERENCES statuses (status_id) ON DELETE CASCADE,
  	FOREIGN KEY status_comments_users_FK (user_id) REFERENCES users (user_id),
  	FOREIGN KEY status_comments_friends_FK (friend_id) REFERENCES users (user_id) ON DELETE CASCADE,
  	UNIQUE KEY status_comment_UK (comment_id, friend_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE videos (
	video_id varchar(255) NOT NULL,
  	user_id varchar(255) NOT NULL,
  	PRIMARY KEY (video_id, user_id),
  	FOREIGN KEY video_id_users_FK (user_id) REFERENCES users (user_id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE video_likes (
	video_id varchar(255) NOT NULL,
  	user_id varchar(255) NOT NULL,
  	friend_id varchar(255) NOT NULL,
	PRIMARY KEY video_likes_PK (video_id, user_id, friend_id),
  	FOREIGN KEY video_likes_photos_FK (video_id) REFERENCES videos (video_id) ON DELETE CASCADE,
  	FOREIGN KEY video_likes_user_id_FK (user_id) REFERENCES users (user_id),
  	FOREIGN KEY video_likes_friends_FK (friend_id) REFERENCES users (user_id) ON DELETE CASCADE,
  	UNIQUE KEY video_likes_UK (video_id, friend_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE video_comments (
  	comment_id varchar(255) NOT NULL,
  	video_id varchar(255) NOT NULL,
  	user_id varchar(255) NOT NULL,
  	friend_id varchar(255) NOT NULL,
	PRIMARY KEY video_comments_PK (comment_id, video_id, user_id, friend_id),
  	FOREIGN KEY video_comments_photos_FK (video_id) REFERENCES videos (video_id) ON DELETE CASCADE,
  	FOREIGN KEY video_comments_users_FK (user_id) REFERENCES users (user_id),
  	FOREIGN KEY video_comments_friends_FK (friend_id) REFERENCES users (user_id) ON DELETE CASCADE,
  	UNIQUE KEY video_comment_UK (comment_id, friend_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;